module Test_linear_molecule_mod
   use pfunit_mod
   use MW_kinds, only: wp
   use MW_box, only: MW_box_t, &
         MW_box_define_type => define_type, &
         MW_box_void_type => void_type, &
         MW_box_minimum_image_distance => minimum_image_distance_2DPBC
   use MW_ion, only: MW_ion_t, &
         MW_ion_define_type => define_type, &
         MW_ion_void_type => void_type
   use MW_molecule, only: MW_molecule_t, &
         MW_molecule_define_type => define_type, &
         MW_molecule_void_type => void_type, &
         MW_molecule_set_linear_parameters => set_linear_parameters
   use MW_linear_molecule, only: &
         MW_linear_molecule_constrain_positions => constrain_positions

contains
   ! ========================================================================
   @test
   subroutine test_positions()
      implicit none
      ! Locals
      ! ------
      integer, parameter :: num_sites = 3
      integer, parameter :: num_constraints = 3

      ! rattle
      real(wp) :: tolerance = 1.0e-6 !< tolerance on constraints realisation


      ! Box
      type(MW_box_t) :: box !< simulation box
      real(wp), parameter :: a = 82.541347382400005_wp
      real(wp), parameter :: b = 82.541347382400005_wp
      real(wp), parameter :: c = 327.65960545299998_wp

      ! Time step
      real(wp), parameter :: dt = 8.26820E+01_wp

      ! Molecules
      type(MW_molecule_t) :: molecule

      ! Ions
      type(MW_ion_t) :: ions(num_sites)
      real(wp) :: xyz_now(num_sites,3)
      real(wp) :: forces_now(3,num_sites)
      real(wp) :: velocity_next(num_sites,3)
      real(wp) :: xyz_next(num_sites,3)


      real(wp) :: drnorm2, dr
      integer :: ia, ib, iconstraint

      ! Setup
      ! -----
      call MW_box_define_type(box, a, b, c)

      ! Sites
      call MW_ion_define_type(ions(1), "N", 1, -0.3980_wp,0.0_wp,0.0_wp,0.0_wp, &
             0.0_wp, 14.01_wp, .true., .false.)
      ions(1)%offset = 0
      call MW_ion_define_type(ions(2), "Im2", 1, 0.1290_wp,0.0_wp,0.0_wp,0.0_wp, &
             0.0_wp, 12.01_wp, .true., .false.)
      ions(2)%offset = 1
      call MW_ion_define_type(ions(3), "Im3", 1, 0.2690_wp,0.0_wp,0.0_wp,0.0_wp, &
             0.0_wp, 15.04_wp, .true., .false.)
      ions(3)%offset = 2

      xyz_now(1,1) = 43.449070396071257_wp
      xyz_now(1,2) = 11.286425024849271_wp
      xyz_now(1,3) = 38.860171538006568_wp

      xyz_now(2,1) = 41.484032186164541_wp
      xyz_now(2,2) = 12.202804671726247_wp
      xyz_now(2,3) = 38.432417898166719_wp

      xyz_now(3,1) = 39.029699461991051_wp
      xyz_now(3,2) = 13.347362850675591_wp
      xyz_now(3,3) = 37.898153602006744_wp

      forces_now(1,1) = 0.0_wp
      forces_now(2,1) = 0.0_wp
      forces_now(3,1) = 0.0_wp

      forces_now(1,2) = 0.0_wp
      forces_now(2,2) = 0.0_wp
      forces_now(3,2) = 0.0_wp

      forces_now(1,3) = 0.0_wp
      forces_now(2,3) = 0.0_wp
      forces_now(3,3) = 0.0_wp

      xyz_next(1,1) = 43.449070396071257_wp
      xyz_next(1,2) = 11.286425024849271_wp
      xyz_next(1,3) = 38.860171538006568_wp

      xyz_next(2,1) = 41.484032186164541_wp
      xyz_next(2,2) = 12.202804671726247_wp
      xyz_next(2,3) = 38.432417898166719_wp

      xyz_next(3,1) = 39.029699461991051_wp
      xyz_next(3,2) = 13.347362850675591_wp
      xyz_next(3,3) = 37.898153602006744_wp

      velocity_next(1,1) = -9.31902343478366080E-005_wp
      velocity_next(1,2) =  4.68084863134379955E-005_wp
      velocity_next(1,3) = -1.31919249127996128E-004_wp

      velocity_next(2,1) = -7.09362090405538960E-005_wp
      velocity_next(2,2) =  8.46326481246942661E-005_wp
      velocity_next(2,3) = -1.53583027999331718E-004_wp

      velocity_next(3,1) = -4.31409314317825771E-005_wp
      velocity_next(3,2) =  1.31875026226958831E-004_wp
      velocity_next(3,3) = -1.80641087809634883E-004_wp

      call MW_molecule_define_type(molecule, "ACN", 1, num_sites, num_constraints, 0, 0, 0, 0, 0, 0, .false., 0.0_wp)
      molecule%sites(1) = 1
      molecule%sites(2) = 2
      molecule%sites(3) = 3

      ! Constraints
      molecule%constrained_sites(1,1) = 1
      molecule%constrained_sites(2,1) = 2
      molecule%constrained_dr(1) = 2.21_wp

      molecule%constrained_sites(1,2) = 1
      molecule%constrained_sites(2,2) = 3
      molecule%constrained_dr(2) = 4.97_wp

      molecule%constrained_sites(1,3) = 2
      molecule%constrained_sites(2,3) = 3
      molecule%constrained_dr(3) = 2.76_wp

      call MW_molecule_set_linear_parameters(molecule, 1, 2)

      ! Test
      ! ----
      call MW_linear_molecule_constrain_positions(2, molecule, dt, box, ions, &
         xyz_now, forces_now, xyz_next, velocity_next)

      ! verify that constraints are satisfied
      do iconstraint = 1, num_constraints
         ia = molecule%constrained_sites(1,iconstraint)
         ib = molecule%constrained_sites(2,iconstraint)
         call MW_box_minimum_image_distance(box%length(1), box%length(2), box%length(3), &
               xyz_next(ia,1), xyz_next(ia,2), xyz_next(ia,3), &
               xyz_next(ib,1), xyz_next(ib,2), xyz_next(ib,3), &
               drnorm2)
         dr = sqrt(drnorm2)
         @assertEqual(molecule%constrained_dr(iconstraint), dr, tolerance*molecule%constrained_dr(iconstraint))
      end do

      ! Tear down
      ! ---------
      call MW_box_void_type(box)
      call MW_molecule_void_type(molecule)
      call MW_ion_void_type(ions(1))
      call MW_ion_void_type(ions(2))
      call MW_ion_void_type(ions(3))

   end subroutine test_positions

end module Test_linear_molecule_mod
