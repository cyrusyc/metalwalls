import unittest
import os.path
import glob
import numpy as np

import mwrun

class test_thomas_fermi(unittest.TestCase):

  @classmethod
  def setUpClass(self):
    self.test_path = "thomas_fermi"
    self.mw_exec = os.path.abspath(mwrun.glob_mw_exec)

  def run_conf(self, confID, nranks):
    path_to_config = os.path.join(self.test_path, confID)
    n = mwrun.mwrun(self.mw_exec, path_to_config)
    self.workdir = n.workdir
    n.run_mw(nranks)

    ok, msg = n.compare_datafiles("charges.out", "charges.ref")
    self.assertTrue(ok, msg)

    ok, msg = n.compare_datafiles("energies_breakdown.out", "energies_breakdown.ref")
    self.assertTrue(ok, msg)

    ok, msg = n.compare_datafiles("forces.out", "forces.ref")
    self.assertTrue(ok, msg)

  def tearDown(self):
    for f in glob.glob(os.path.join(self.workdir, "hessian_matrix.inpt")):
      os.remove(f)
    for f in glob.glob(os.path.join(self.workdir, "maze_matrix.inpt")):
      os.remove(f)
    for f in glob.glob(os.path.join(self.workdir, "*.out")):
      os.remove(f)

  def test_conf_thomas_fermi_LiCl_Al_cg(self):
    self.run_conf("LiCl-Al-cg", 4)

  def test_conf_thomas_fermi_LiCl_Al_inv(self):
    self.run_conf("LiCl-Al-inv", 4)

  def test_conf_thomas_fermi_LiCl_Al_maze_inv(self):
    self.run_conf("LiCl-Al-maze-inv", 4)

  def test_conf_thomas_fermi_LiCl_Al_maze_cg_nostore(self):
    self.run_conf("LiCl-Al-maze-cg-nostore", 4)

