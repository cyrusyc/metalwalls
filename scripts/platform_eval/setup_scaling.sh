
CWD=$PWD
ref_dir=/ccc/work/cont003/mds/haefemat/runs/blue-energy/prod/2018-12-12/tuning_loop_block_size/template

#for BLOCK in 8 16 32 64 128
for BLOCK in 128 256 512 1024 2048 4096 8192
do
  echo "Treating Block $BLOCK"
  mkdir block$BLOCK
  cd block$BLOCK
  export BLOCK
  for NODE in 1 2
  do
    mkdir node$NODE
    cd node$NODE
    ln $ref_dir/runtime.inpt .
    ln $ref_dir/data.inpt .
    export NODE
    TASK=$(( NODE * 48 )) envsubst < $ref_dir/job.template > job
    ccc_msub job > job_id
    cd ..
  done
  cd ..
done

