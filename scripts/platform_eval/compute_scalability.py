
f = open("res.out", "r")
lines = f.readlines()[1:]
f.close()
res = []
for l in lines:
  r = l.split()
  res.append([int(r[0]), float(r[1])])

serial_time = res[0][1]
for i in res:
  i.append(serial_time / i[1])
  i.append(serial_time / i[1] / i[0] * 100)

f = open("scaling", "w")
f.write("|#Nodes |  Time(s) |  Speedup  |  Eff(%)|\n")
f.write("|:-----:|:--------:|:---------:|:------:|\n")
for i in res:
  f.write("|%6d | %8.2f | %8.2f | %8.1f|\n"%(i[0], i[1], i[2], i[3]))
f.close()


