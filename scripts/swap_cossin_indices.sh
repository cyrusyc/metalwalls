#! /usr/local/bin/bash
# Sed script to efficiently swap indices order for cos/sin arrays
# It can be useful to test the efficiency of some data layout against another one
SRC_DIR=../src/
TEST_DIR=${SRC_DIR}/tests/

ARRAYS="cossin_kx_ions cossin_ky_ions cossin_kz_ions cossin_kx_elec cossin_ky_elec cossin_kz_elec"
SRC_FILES="coulomb.f90 ewald.f90"
TEST_FILES="test_ewald.f90"

for f in ${SRC_FILES}
do
    cp ${SRC_DIR}/${f} tmp1
    for a in ${ARRAYS}
    do
        sed "s/${a}(\([:0-9a-zA-Z_][:%0-9a-zA-Z_]*\),\([:0-9a-zA-Z_][:%0-9a-zA-Z_]*\),\([:0-9a-zA-Z_][:%0-9a-zA-Z_]*\)/${a}(\2,\3,\1/g" < tmp1 > tmp2
        mv tmp2 tmp1
    done
    mv tmp1 ${f}
done

for f in ${TEST_FILES}
do
    cp ${TEST_DIR}/${f} tmp1
    for a in ${ARRAYS}
    do
        sed "s/${a}(\([:0-9a-zA-Z_][:%0-9a-zA-Z_]*\),\([:0-9a-zA-Z_][:%0-9a-zA-Z_]*\),\([:0-9a-zA-Z_][:%0-9a-zA-Z_]*\)/${a}(\2,\3,\1/g" < tmp1 > tmp2
        mv tmp2 tmp1
    done
    mv tmp1 ${f}
done

