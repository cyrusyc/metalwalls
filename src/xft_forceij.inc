   ! ! ================================================================================
   ! ! Function to compute force coeffcient between 2 particles
   ! !
   ! ! ft_ij = (dV(r_ij) / dr_ij) * (1/ r_ij)
   ! pure function force_ij(drnorm2, eta_ij, B_ij, n_ij, C_ij, D_ij, damp_dd_ij, damp_dq_ij) result(ft_ij)
   !    implicit none
   !    ! Passed in
   !    ! ---------
   !
   !    real(wp), intent(in) :: drnorm2
   !    real(wp), intent(in) :: eta_ij
   !    real(wp), intent(in) :: B_ij
   !    real(wp), intent(in) :: C_ij
   !    real(wp), intent(in) :: D_ij
   !    real(wp), intent(in) :: damp_dd_ij
   !    real(wp), intent(in) :: damp_dq_ij

   !    ! Result
   !    ! ------
   !    real(wp) :: ft_ij

   !    ! Local
   !    ! -----

   !    real(wp) :: drnorm, drnormrec, drnorm2rec, drnorm6rec, drnorm8rec
   !    real(wp) :: damp_dd_sum, damp_dd_term
   !    real(wp) :: damp_dq_sum, damp_dq_term
   !    integer :: k, l
      drnorm = sqrt(drnorm2)
      drnormrec  = 1.0_wp / drnorm
      drnormnrec = drnormrec**n_ij
      drnorm2rec = drnormrec * drnormrec
      drnorm6rec = drnorm2rec * drnorm2rec * drnorm2rec
      drnorm8rec = drnorm2rec * drnorm6rec

      ! damp_dd_sum = sum_{k=0}^{6} (d_{dd}^k r_{ij}^k) / k!
      damp_dd_sum = 1.0_wp
      damp_dd_term = 1.0_wp
      do k = 1, 6
         damp_dd_term = damp_dd_term * (damp_dd_ij * drnorm / real(k,wp)) ! damp_dd_term =  (d_{dd}^k r_{ij}^k) / k!
         damp_dd_sum = damp_dd_sum + damp_dd_term
      end do

      ! damp_dq_sum = sum_{k=0}^{8} (d_{dq}^k r_{ij}^k) / k!
      damp_dq_sum = 1.0_wp
      damp_dq_term = 1.0_wp
      do l = 1, 8
         damp_dq_term = damp_dq_term * (damp_dq_ij * drnorm / real(l,wp)) ! damp_dq_term =  (d_{dq}^k r_{ij}^k) / k!
         damp_dq_sum = damp_dq_sum + damp_dq_term
      end do

      xft_ij = -eta_ij * B_ij * exp(-eta_ij*drnorm)*drnormnrec &
            -  B_ij * exp(-eta_ij*drnorm) * drnormnrec * drnormrec * real(n_ij,wp) &
            -  Bx_ij * 2.0_wp * drnorm * exp(-etax_ij*drnorm2) &
            + (C_ij * drnorm6rec) * (6.0_wp * drnormrec * (1.0_wp - damp_dd_sum*exp(-damp_dd_ij*drnorm)) &
            - damp_dd_ij * damp_dd_term * exp(-damp_dd_ij * drnorm)) &
            + (D_ij * drnorm8rec) * (8.0_wp * drnormrec * (1.0_wp - damp_dq_sum*exp(-damp_dq_ij*drnorm)) &
            - damp_dq_ij * damp_dq_term * exp(-damp_dq_ij * drnorm))

      xft_ij = xft_ij * drnormrec

   !   end function force_ij
