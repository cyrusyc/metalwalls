module purge
# GNU
# ---
# module load gnu-env/5.4.0
# module load openmpi/1.10.0_gnu54
# Intel
# -----
module load intel-env/15.0.0
module load openmpi/1.6.3_intel15.0.0
module load python
