
if [ $# -ne 1 ] 
then
   echo 'Specify desired environment: gnu, intel2018 or intel2019'
elif [ $1 = 'gnu' ]
then
   # Gnu
   module load Core/Gnu/6.3.0 Libraries/openmpi/2.1.2/Intel/2018-x86_64 Libraries/lapack/lapack-3.8.0/Gnu/6.3.0-x86_64
elif [ $1 = 'intel2018' ]
then
   # Intel 2018 + Plumed
   module load Core/Intel/2018 Core/Gnu/6.3.0 Libraries/impi/2018 Libraries/mkl/2018 Libraries/plumed/Intel/2.5.1/2018
elif [ $1 = 'intel2019' ]
then
   # Intel 2019 + Plumed + Python
   module load Core/Intel/2019 Core/Gnu/6.3.0 Libraries/impi/2019 Libraries/mkl/2019 Libraries/plumed/Intel/2.5.1/2019 Libraries/IntelPython/2019
else
   echo "Error: $1 environment not recognized, please specify the desired environment: gnu, intel2018 or intel2019"
fi

export OMPI_MCA_btl_base_warn_component_unused=0
