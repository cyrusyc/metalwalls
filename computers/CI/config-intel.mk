# Compilation options
F90 := mpiifort
F90FLAGS := -O0 -g -check bounds,arg_temp_created,assume,contiguous,format,shape,stack,uninit -traceback -diag-disable=remark -no-wrap-margin -fPIC -mkl=cluster
FPPFLAGS := -DMW_CI -DMW_USE_PLUMED
LDFLAGS := 
F2PY := f2py
F90WRAP := f90wrap
FCOMPILER := intelem
J := -module

# Path to pFUnit (Unit testing Framework)
PFUNIT := $(PFUNIT)
