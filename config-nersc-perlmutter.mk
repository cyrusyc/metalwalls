F90 := pgfortran
F90FLAGS := -fast -Mvect -m64 -Minfo=ccff -Mpreprocess -g
F90FLAGS := -tp=px -Minfo=ccff -Mpreprocess
F90FLAGS += -acc -ta=tesla:managed -Minline
F90FLAGS += -Minfo=accel,inline
FPPFLAGS := -DMW_SERIAL
LDFLAGS := -llapack -lblas
J := -I

F2PY := f2py
F90WRAP := f90wrap


