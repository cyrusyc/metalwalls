cmake_minimum_required (VERSION 3.0.2)
project (Metalwalls Fortran)
include(CTest)
include (${PROJECT_SOURCE_DIR}/config.cmake)
set(CMAKE_Fortran_FLAGS_RELEASE ${MW_F90_OPT_FLAGS})
set(MW_RELEASE 18.07)

option (BUILD_TEST "Build unit tests suite (requires PFUnit)" OFF) 
option (PFUNIT_INSTALL "Directory where pFUnit is installed" OFF)
option (GEN_RELEASE "Changes the welcome message in run.out to show that this version is tagged in the repository" OFF)
option (WITH_MPI "Build Metalwalls with MPI" ON)


set(default_build_type "Release")
if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to '${default_build_type}' as none was specified.")
  set(CMAKE_BUILD_TYPE "${default_build_type}" CACHE STRING "Choose the type of build." FORCE)
  # Set the possible values of build type for cmake-gui
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release")
endif()

message ( STATUS "BUILD_TEST = ${BUILD_TEST}")        
message ( STATUS "PFUNIT_INSTALL = ${PFUNIT_INSTALL}")        
message ( STATUS "GEN_RELEASE = " ${GEN_RELEASE})        
message ( STATUS "WITH_MPI = " ${WITH_MPI})        

if (BUILD_TEST)
  #Pre-processor flag to set to compile the constant.f90 with the right numbers otherwise unit-test on localwork break
  set ( CI_OPTION -DMW_CI )
endif()

add_subdirectory(src)
add_subdirectory(tests/pFUnit)
       
