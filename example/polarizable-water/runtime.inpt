# System configuration
# ====================

# Global simulation parameters
# ----------------------------
num_steps       1 # number of steps in the run
timestep     41.341 # timestep (a.u)
temperature   298.0 # temperature (K)

# Periodic boundary conditions
# ----------------------------
num_pbc  3

# Thermostat:
# -----------
thermostat
  chain_length  5
  relaxation_time 4134.1
  tolerance  1.0e-17
  max_iteration  100

# Species definition
# ------------------
species

  species_type
    name           O            # name of the species
    count          267          # number of species      
    charge point   -0.73        # permanent charge on the ions
    mass           15.9994      # mass in amu
    polarizability 3.56312      # polarizability (a.u.)

  species_type
    name           H1           # name of the species    
    count          267          # number of species     
    charge point   0.365        # permanent charge on the ions
    mass           1.008        # mass in amu
    polarizability 1.14722      # polarizability (a.u.)

  species_type
    name           H2           # name of the species
    count          267          # number of species     
    charge point   0.365        # permanent charge on the ions
    mass           1.008        # mass in amu
    polarizability 1.14722      # polarizability (a.u.)

  dipoles_minimization cg 1.0e-6 100  # choice of algorithm for atomic dipoles

# Molecule definitions
# --------------------
molecules

  molecule_type
    name     POL3  # name of molecule       
    count    267   # number of molecules
    sites O H1 H2  # molecule's sites

    # Rigid constraints
    constraint O   H1   1.88972   # constrained radius for a pair of sites (bohr)
    constraint O   H2   1.88972   # constrained radius for a pair of sites (bohr)
    constraint H1  H2   3.08587   # constrained radius for a pair of sites (bohr)

    constraints_algorithm rattle 1.0e-7 100

# Interactions definition
# -----------------------
interactions
  coulomb
    coulomb_rtol       2.08321e-05     # coulomb rtol
    coulomb_rcut       18.89726124565  # coulomb cutoff (bohr)
    coulomb_ktol       2.38756e-04     # coulomb ktol

  lennard-jones
     lj_rcut 18.8972612456506          # lj cutoff (bohr)
     # lj parameters: epsilon in kJ/mol, sigma in angstrom
     lj_pair   O       O  0.655     3.204  
 
  damping
     tt_pair  O   O     2.0 4 0.0
     tt_pair  O   H1    2.0 4 0.0
     tt_pair  O   H2    2.0 4 0.0
     tt_pair  H1  O     2.0 4 0.0
     tt_pair  H1  H1    2.0 4 0.0
     tt_pair  H1  H2    2.0 4 0.0
     tt_pair  H2  O     2.0 4 0.0
     tt_pair  H2  H1    2.0 4 0.0
     tt_pair  H2  H2    2.0 4 0.0

output
  default 1
  
